﻿using UnityEngine;
using System.Collections;

public class BowArmController : MonoBehaviour
{
 
    private bool inverted = false;
    void Start()
    {
    }

    void Update()
    {

        float xinput = Input.mousePosition.x;
        float yinput = Input.mousePosition.y;

        Vector3 mouseScreen = Camera.main.ScreenToWorldPoint(new Vector3(xinput, yinput, Input.mousePosition.z));

        var pos = GameObject.Find("Player").transform.position;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, GameObject.Find("Bow").transform.rotation, 10);
    }
}
