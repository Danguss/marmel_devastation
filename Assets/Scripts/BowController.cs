﻿using UnityEngine;
using System.Collections;

public class BowController : MonoBehaviour {


	//private bool facingRight = true;
	//Animator anim;
	public Transform target;
    private bool inverted = false;
	void Start ()
	{
		//anim = GetComponent<Animator> ();
	}

	void Update()
	{
        //transform.Rotate(0,0,Time.deltaTime);new Vector3(Input.mousePosition

        float xinput = Input.mousePosition.x;
        float yinput = Input.mousePosition.y;
        //yinput += 50;
        //xinput -= 140;
        Vector3 mouseScreen = Camera.main.ScreenToWorldPoint(new Vector3(xinput, yinput, Input.mousePosition.z));
        
		transform.rotation = Quaternion.LookRotation (Vector3.forward, mouseScreen - transform.position);
        transform.rotation *= Quaternion.Euler(0, 0, 130);
        var pos = GameObject.Find("Player").transform.position;

        float move = Input.GetAxis("Horizontal");
        if (pos.x > mouseScreen.x && move == 0)
        {
            //inverted = true;
            transform.rotation *= Quaternion.Euler(0, 0, 100);
            transform.rotation = Quaternion.Inverse(transform.rotation);
        }
        else if (move < 0)
        {
            transform.rotation *= Quaternion.Euler(0, 0, 100);
            transform.rotation = Quaternion.Inverse(transform.rotation);
        }
        
    }
}
