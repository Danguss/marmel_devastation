﻿using UnityEngine;
using System.Collections;

public class CollisionScript : MonoBehaviour {

    public Transform explosionPrefab;
    private Rigidbody2D arrow;
	public AudioClip shotHitClip;
	AudioSource hitSound;
    private int collisionCount = 0;
	// Use this for initialization
	void Start () {
        arrow = GetComponent<Rigidbody2D>();
		hitSound = GetComponent<AudioSource>();
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 dir = arrow.velocity;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (arrow.velocity != new Vector2(0, 0))
        {
            arrow.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    public void OnCollisionEnter2D(Collision2D coll)
    {
        if (collisionCount == 0)
        {
            hitSound.PlayOneShot(shotHitClip);
            var arrow = GetComponent<Rigidbody2D>();
            if ((coll.gameObject.name == "Box" || coll.gameObject.name == "Box(Clone)") && arrow.gameObject.name == "Arrow3(Clone)")
            {
                Destroy(coll.gameObject);
                Destroy(arrow.gameObject);
            }


            arrow.freezeRotation = true;
            arrow.velocity = new Vector2(0, 0);
            //arrow.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionX;
            //arrow.transform.Translate(new Vector2(0, 0));//stop cloned spear moving
            //arrow.transform.Rotate(new Vector2(0, 0));//stop cloned spear rotating
            arrow.gravityScale = 0.0f; //stop it falling to ground
            collisionCount++;
        }

        if (coll.gameObject.tag == "brick")
        {
            var boom = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            
        }
    }
}
