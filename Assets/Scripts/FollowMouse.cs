﻿using UnityEngine;
using System.Collections;

public class FollowMouse : MonoBehaviour {

	private float m_depth;

	void Start() {
		m_depth = transform.position.z - Camera.main.transform.position.z;
	}

	void Update() {
		transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,
			Input.mousePosition.y,
			m_depth));
	}
}
