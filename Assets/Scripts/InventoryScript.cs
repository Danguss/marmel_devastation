﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryScript : MonoBehaviour
{
    public int arrowCount = 5;
    public int currentArrow = 1;
    public Sprite[] InventorySprites;
    public Image InventoryUI;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.mouseScrollDelta.y != 0)
        {
            var change = Input.mouseScrollDelta.y;
            currentArrow -= int.Parse(change.ToString());
            if(currentArrow > arrowCount)
            {
                currentArrow -= arrowCount;
            }
            if (currentArrow < 1)
            {
                currentArrow = arrowCount;
            }
            InventoryUI.sprite = InventorySprites[currentArrow-1];
            
        }
    }
}
