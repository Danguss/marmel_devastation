﻿using UnityEngine;
using System.Collections;

public class LevelSelect2 : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter2D(Collider2D other) {
		var box = GetComponent<Rigidbody2D>();
		if (other.gameObject.name == "Player") {
			Application.LoadLevel ("Level2"); 
		}
	}
}
