﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

    public Button startText;
    public Button exitText;
    public Button sound;

    void Start()
    {
        startText = startText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        sound = sound.GetComponent<Button>();
    }

	public void LoadScene () {
		SceneManager.LoadScene ("LvlSelect.V2");
	}

	public void quitGame () {
		Application.Quit ();
	}
    public void soundOnOff()
    {
        if (AudioListener.volume != 0)
            AudioListener.volume = 0;
        else if (AudioListener.volume == 0)
            AudioListener.volume = 3;
    }
}