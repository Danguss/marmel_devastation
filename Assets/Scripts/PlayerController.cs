﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float maxSpeed = 10f;
	private bool facingRight = true;
    private bool bowFliped = false;
	public float jumpForce = 5f;
	public AudioClip teleClip;
	public AudioClip jumpDownClip;
	AudioSource runSound;
	Animator anim;

	private Rigidbody2D myrigidbody;

	bool grounded  = false;
	public Transform groundCheck;
	float groundRadius = 0.2f;
	public LayerMask whatIsGround;
    private GameObject block;



	// Use this for initialization
	void Start () {
		myrigidbody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		runSound = GetComponent<AudioSource>();
        block = Resources.Load("Box") as GameObject;
        block.name = "Box";
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
		anim.SetBool ("Ground", grounded);
		anim.SetFloat ("vSpeed", myrigidbody.velocity.y);

        
        if (Input.GetMouseButtonDown(1))
        {

            var test = GetComponent<ProjectileScript>().GetLastProjectile();

            if (test.gameObject.name == "Arrow1(Clone)")
            {
				runSound.PlayOneShot (teleClip);
                myrigidbody.position = test.position;
            }
            if (test.gameObject.name == "Arrow4(Clone)")
            {
                Instantiate(block, test.transform.position, Quaternion.identity);
            }

            //if (script.getArrow() == 1)
            //{

            //}
            //if (script.getArrow() == 2)
            //{
            //    myrigidbody.velocity = arrow.position - myrigidbody.position;
            //}
            //if (script.getArrow() == 3)
            //{

            //}
            //if (script.getArrow() == 4)
            //{

            //}
            //if (script.getArrow() == 5)
            //{

            //}

        }
        Vector3 mouseScreen = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        

        float move = Input.GetAxis("Horizontal");

		anim.SetFloat ("Speed", Mathf.Abs (move));

		myrigidbody.velocity = new Vector2(move * maxSpeed, myrigidbody.velocity.y);


        if (move == 0 && transform.position.x > mouseScreen.x && !bowFliped)
        {
            bowFliped = true;
            Flip();
        }
        else if (move == 0 && transform.position.x < mouseScreen.x && bowFliped)
        {
            bowFliped = false;
            Flip();
        }
        else if (move > 0 && !facingRight)
        {
            bowFliped = false;
            Flip();
        }
        else if (move < 0 && facingRight)
        {
            bowFliped = true;
            Flip();
        }
	}

	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void Update()
	{
		if (grounded && Input.GetKeyDown (KeyCode.Space)) {
			anim.SetBool ("Ground", false);
			myrigidbody.AddForce (new Vector2 (0, jumpForce));
		}
		if (grounded && (Input.GetKeyDown (KeyCode.A) || Input.GetKeyDown (KeyCode.D))) {
			runSound.Play ();
			Debug.Log ("Sound!");
		} 
		if ((Input.GetKeyUp (KeyCode.A) || Input.GetKeyUp (KeyCode.D))) {
			runSound.Stop ();
		}
	}

	public void OnCollisionEnter2D(Collision2D coll)
	{
		if (!grounded) {
			runSound.PlayOneShot (jumpDownClip);
			Debug.Log ("Down!");
		}
	}
}
