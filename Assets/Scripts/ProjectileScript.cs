﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ProjectileScript : MonoBehaviour
{
    
    //public float speed = 5.0f;
    private int Count;
    private Rigidbody2D prefab;
    private Rigidbody2D player;
    private GameObject GO;
    private float lastPosX;
    private float lastPasY;
	public AudioClip shotClip;
	AudioSource shotSound;
    //private bool stuck;
    //private int arrowType;
    // Use this for initialization

    void Start()
    {
		shotSound = GetComponent<AudioSource> ();
        Count = 0;
    }

    // Update is called once per frame
    public Rigidbody2D projectile;
    public List<Rigidbody2D> projectileList = new List<Rigidbody2D>();
    private Rigidbody2D arrow;
    void Update()
    {
        var nr = GameObject.Find("Main Camera").GetComponent<InventoryScript>().currentArrow;
        GO = Resources.Load("Arrow" + nr.ToString()) as GameObject;
        //prefab = GameObject.Find("arrow").GetComponent<Rigidbody2D>();
        prefab = GO.GetComponent<Rigidbody2D>();
        player = GetComponent<Rigidbody2D>();

        if (Input.GetMouseButtonDown(0))
        {	
			shotSound.PlayOneShot (shotClip);

            var playerPosition = player.transform.position;
            var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var shootingDirection = mousePosition - playerPosition;
            arrow = Instantiate(prefab, player.transform.position, player.transform.rotation) as Rigidbody2D;
            arrow.velocity = shootingDirection * 3;
            projectileList.Add(arrow);
            Count++;
        }

    }

    public Rigidbody2D GetLastProjectile()
    {
        return projectileList[Count - 1];
    }
    //public int getArrow()
    //{
    //    return arrowType;
    //}
    //public void setArrow(int type)
    //{
    //    arrowType = type;
    //}

    public void OnCollisionEnter2D(Collision2D coll)
    {
        
        Debug.Log("aaaa");
    }
}
