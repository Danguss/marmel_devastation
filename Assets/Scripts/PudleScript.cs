﻿using UnityEngine;
using System.Collections;


public class PudleScript : MonoBehaviour {

	public Transform explosionPrefab;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll){
		
		if (coll.gameObject.tag == "Player") {  
			var boom = Instantiate (explosionPrefab, transform.position, Quaternion.identity);
			Destroy(boom, 0.1f); // also sometimes destroying right away causes problems
		}
	}



}
