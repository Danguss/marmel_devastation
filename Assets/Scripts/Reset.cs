﻿using UnityEngine;
using System.Collections;

public class Reset : MonoBehaviour {

	public Transform player;
	public float x;
	public float y;
	public float z;


	void Start()
	{

	}
	// Use this for initialization
	void OnTriggerEnter2D(Collider2D other) {
		var box = GetComponent<Rigidbody2D>();
		if (other.gameObject.name == "Player") {
			player.position = new Vector3 (x, y, z);
		}
	}
}
