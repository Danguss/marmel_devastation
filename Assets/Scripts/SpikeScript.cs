﻿using UnityEngine;
using System.Collections;

public class SpikeScript : MonoBehaviour {

    public GameObject spikes;

    public float moveSpeed;

    private Transform currentPoint;

    public float time;

    public Transform[] points;

    public int pointSelection;

    private bool check = true;
	// Use this for initialization
	void Start () {
        currentPoint = points[pointSelection];
	
	}

    // Update is called once per frame
    void Update() {

        spikes.transform.position = Vector3.MoveTowards(spikes.transform.position, currentPoint.position, Time.deltaTime * moveSpeed);

        if (spikes.transform.position == currentPoint.position && check == true)
        {
            check = false;
            StartCoroutine(countTime());
            //pointSelection++;
            //if (pointSelection == points.Length)
            //    pointSelection = 0;

            //currentPoint = points[pointSelection];
        }

        
    }
    IEnumerator countTime()
    {
        yield return new WaitForSeconds(time);
        pointSelection++;
        if (pointSelection == points.Length)
            pointSelection = 0;

        currentPoint = points[pointSelection];
        check = true;
    }
}
